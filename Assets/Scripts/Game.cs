﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{

    // This array will handle Duck count on the screen and will check when ducks have either been shoot or escaped
    public GameObject duck;
    public List<GameObject> DuckList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    // This function handles spawning of one or two ducks based on how many you need
    // on the screen
    public void spawnDucks()
    {
        // This section handles the position calculating
        float x = Random.Range(-15, 2); // Duck will spawn in Horizontal Axis from -15 to positive 2 cordinates
        float y = 7.0f; // Ducks will always spawn at +7 Y axis to show up on map
        float z = 0; // Ducks will spawn at Z Layer 0 to be behind the bushes but in front of Background Sky

        // Spawn a duck and add it to a list
        DuckList.Add(Instantiate(duck, new Vector3(x, y, z), Quaternion.identity));
    }


}
